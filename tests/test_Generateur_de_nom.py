import unittest
from Generateur_de_nom import * #Permet d'importer le fichier principale
from Univers import * #Permet d'importer les univers 


class TestGenerateurDeNom(unittest.TestCase):


    def test_choix_univer(self):
        self.assertEqual(choix_univer(), ("star_wars_nom","star_wars_prenom"))

    def test_transformation_ASCII(self):
        self.assertEqual(generation_nomprenom_aleatoire("star_wars_nom","star_wars_prenom"), ("Greedo","The hutt")) #Résultat du test avec comme entré de nom Guillaume Fessard
