from Univers import * #Permet d'importer les univers.
from tkinter import * #Import de tkinter

#if(len(nom) > 1 | len(prenom) > 4 ) : serait utiliser pour vérifier la saisie mais ne fonctionne pas

#Fonction permettant que lors du click sur le boutton que le nom et prenom soient généré 
def button_generation():

    if(univer.get() != "Choix univers"):
        
        #Appelle de la fonction choix_univer
        nom_univer,prenom_univer = choix_univer()

        #Appelle de la fonction de génération de nom
        nom_genere,prenom_genere = generation_nomprenom_aleatoire(nom_univer,prenom_univer)

        #Affichage du nom généré
        Sortie.set("%s %s" % (nom_genere, prenom_genere))
        
    else :
        Sortie.set("Pas d'univers saisie")

#Fonction permettant de générer des nom et prénom aléatoire suivant l'univers
def generation_nomprenom_aleatoire(nom_univer,prenom_univer) :

    nom = Nom.get()
    prenom = Prenom.get()

    nom_genere = liste_univer[nom_univer][nom[0].upper()]
    prenom_genere = liste_univer[prenom_univer][prenom[2].upper()]
    return nom_genere,prenom_genere

#Fonction qui permet de mettre en forme l'univers 
def choix_univer():

    nom_univer = "%s_nom" % (univer.get())
    prenom_univer = "%s_prenom" % (univer.get())
    return nom_univer,prenom_univer


#Création de la fenêtre
fenetre = Tk()
fenetre.title("Générateur de nom")

#fenetre.geometry("500x500")

#Déclaration des variables de text
Nom = StringVar()
Nom.set('Saisir votre Nom')

Prenom = StringVar()
Prenom.set('Saisir le Prenom')

Sortie = StringVar()
Sortie.set('Text de sortie')

listeUniver = ('star_wars', 'game_of_thrones')
univer = StringVar()
univer.set("Choix univers")

#Label affichant le Titre
titre = Label(fenetre, text='Générateur de nom')
titre.grid(column=3,row=0)
#titre.place(x=200,y=50)

#Zone de text pour la saisie du nom de l'utilisateur
input_Nom = Entry(fenetre, textvariable=Nom)
input_Nom.grid(column=2, row=3, padx=10,pady=10, ipady=20)

#Zone de text pour la saisie du prenom de l'utilisateur
input_Prenom = Entry(fenetre, textvariable=Prenom)
input_Prenom.grid(column=3, row=3, padx=10,pady=10, ipady=20)

#Bouton permettant de générer le nom et prénom
button_Chiffrer = Button(fenetre, text="Générer !", command=button_generation)
button_Chiffrer.grid(row=4, column=3, padx=15,pady=5, ipady=10)

#Menu déroulant avec les différent univers
om = OptionMenu(fenetre, univer, *listeUniver)
om.grid(row=3, column = 4, padx=15,pady=5, ipady=10)

#Zone de sortie
output_sortie = Label(fenetre, textvariable=Sortie)
output_sortie.grid(column=3, row=7, padx=10,pady=10, ipady=20)

fenetre.mainloop()